package com.tomasmichalkevic.testapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

// 1. This is just a class. Most of the things won't work. Make it an activity by extending the AppCompatActivity.
// Then try to uncomment the blocks below
// 2. Read about the Android Activity lifecycles
// 3. Use the Synthetic lib from kotlinx, it gives the ability to directly affect properties in the layout xml. Use this to set the main
// screen text to newText
// 4. Use the lifecycle function onPause to show a Toast message(Google this for Kotlin). If done correctly, when you press home, a message
// should show on the screen
// 5. You have a text on the bottom in the main activity. It has a hardcoded text. This is bad. Extract it to the string.xml
// 6. Read about ConstraintLayout
// 7. Create a FloatingActionButton and then use the ConstraintLayout to position the FAB on any corner you like
// 8. Using the knowledge about the Synthetic, add an OnClickListener to it to again show any text you like as a Toast when you tap on it.
// 9. Create a data class called Movie. Initialise there a variable called title as val.
// 10. Create an onClickListener for a textView(the matrix one) and make it show a toast with a value you set in the Movie data class
// 11. After you tapped it already 5 times, make it change the Toast "Titanic"
// 12. "Implement" the interface EssentialFunctionsInterface. You can use those functions if you want. You don't need to implement the actual
// functions. This is basically to show you how an interface works. The OnClickListener is also an interface

class MainActivity : AppCompatActivity(), EssentialFunctionsInterface{
    override fun changeText() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun changeToast() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun fixSomething(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val newText = "This in an app for Slava"
    private var counter = 0

    //starts application and creates the view that is presented to user
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main) //This ties the Activity to the layout file
        setSupportActionBar(toolbar)

        mainText.text = newText //replaces the text in field with id mainText

        //8. display simple Toast message on click
        floatingActionButton.setOnClickListener {
            Toast.makeText(this@MainActivity, R.string.simple_button, Toast.LENGTH_LONG).show()
        }

        //10.
//        mainText.setOnClickListener{
//            Toast.makeText(this@MainActivity, Movie().title, Toast.LENGTH_SHORT).show()
//        }


        //11.
        mainText.setOnClickListener{
            if(counter > 4) {
                var newMovie = Movie("Titanic")
                Toast.makeText(this@MainActivity, newMovie.title, Toast.LENGTH_SHORT).show()
            } else{
                Toast.makeText(this@MainActivity, Movie().title, Toast.LENGTH_SHORT).show()
            }
            counter++
        }

//        mainText.setOnClickListener(View.OnClickListener() {
//            fun OnClick(v: View) {
//                Toast.makeText(this@MainActivity, Movie().title, Toast.LENGTH_SHORT).show()
//            }
//        })
    }

    //9. data class
    data class Movie(var title: String = "Home alone")

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    //4. Message on HOME button press
    override fun onPause() {
        super.onPause()
        Toast.makeText(this@MainActivity,R.string.random_toast_message, Toast.LENGTH_LONG).show()
    }
}
