package com.tomasmichalkevic.testapp

interface EssentialFunctionsInterface {

    fun changeText()
    fun changeToast()
    fun fixSomething(): String

}